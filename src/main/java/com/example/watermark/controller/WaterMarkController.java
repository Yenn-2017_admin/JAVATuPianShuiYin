package com.example.watermark.controller;

import com.example.watermark.service.IMarkService;
import com.example.watermark.service.MarkServiceFactory;
import com.example.watermark.service.UploadService;
import com.example.watermark.service.impl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class WaterMarkController {
    private final Logger logger = LoggerFactory.getLogger(WaterMarkController.class);

    @Autowired
    private UploadService uploadService;

    @Autowired
    private SingleMarkService singleMarkService;

    @Autowired
    private ImageMarkService imageMarkService;

    @Autowired
    private MoreMarkService moreMarkService;

    @Autowired
    private MoreImageMarkService moreImageMarkService;


    /**
     * 到达首页；
     * @return
     */
    @GetMapping(value = {"/","/index"})
    public String toIndexPage(){
        return "/index";
    }
    /**
     * 做水印；
     * @return
     */
    @PostMapping(value = "/watermark/{type}")
    public String doAndToWatermark0(@RequestParam(value = "image") MultipartFile image,
            @PathVariable(value = "type")String type
            , HttpServletRequest request
            , Model model){
        String uploadPath = "/images";

        //S1-获取绝对路径；
        HttpSession session = request.getSession();
        String realUploadPath = session.getServletContext().getRealPath(uploadPath);
        this.logger.info("image={}",image.getOriginalFilename());
        this.logger.info("realUploadPath={}",realUploadPath);

        if(image==null){
            this.logger.info("image={}",image);
        }

        String imageUrl = uploadService.uploadImage(image,uploadPath,realUploadPath);

//        /*添加单个文字水印*/
//        String watermarkerImageUrl = this.singleMarkService.watermark(image,uploadPath,realUploadPath);

//        /*添加单个图片水印*/
//        String watermarkerImageUrl = this.imageMarkService.watermark(image,uploadPath,realUploadPath);

//        /*添加多个文字水印*/
//        String watermarkerImageUrl = this.moreMarkService.watermark(image,uploadPath,realUploadPath);

        /*添加多个图片水印*/
//        String watermarkerImageUrl = this.moreImageMarkService.watermark(image,uploadPath,realUploadPath);
        String watermarkerImageUrl = MarkServiceFactory.getMarkServiceByClassKey(type).watermark(image,uploadPath,realUploadPath);

        this.logger.info("imageUrl={}",imageUrl);
        this.logger.info("watermarkerImageUrl={}",watermarkerImageUrl);
        model.addAttribute("imageUrl",imageUrl);
        model.addAttribute("watermarkerImageUrl",watermarkerImageUrl);

        return "/watermark";
    }
    /**
     * 做水印；
     * @return
     */
    @PostMapping(value = "/watermark")
    public String doAndToWatermark(@RequestParam(value = "image") MultipartFile image
            , HttpServletRequest request
            , Model model){
        String uploadPath = "/images";

        //S1-获取绝对路径；
        HttpSession session = request.getSession();
        String realUploadPath = session.getServletContext().getRealPath(uploadPath);
        this.logger.info("image={}",image.getOriginalFilename());
        this.logger.info("realUploadPath={}",realUploadPath);

        if(image==null){
            this.logger.info("image={}",image);
        }

        String imageUrl = uploadService.uploadImage(image,uploadPath,realUploadPath);

//        /*添加单个文字水印*/
//        String watermarkerImageUrl = this.singleMarkService.watermark(image,uploadPath,realUploadPath);

//        /*添加单个图片水印*/
//        String watermarkerImageUrl = this.imageMarkService.watermark(image,uploadPath,realUploadPath);

//        /*添加多个文字水印*/
//        String watermarkerImageUrl = this.moreMarkService.watermark(image,uploadPath,realUploadPath);

        /*添加多个图片水印*/
//        String watermarkerImageUrl = this.moreImageMarkService.watermark(image,uploadPath,realUploadPath);
        String watermarkerImageUrl = MarkServiceFactory.getMarkServiceByClassKey("MoreImage").watermark(image,uploadPath,realUploadPath);

        this.logger.info("imageUrl={}",imageUrl);
        this.logger.info("watermarkerImageUrl={}",watermarkerImageUrl);
        model.addAttribute("imageUrl",imageUrl);
        model.addAttribute("watermarkerImageUrl",watermarkerImageUrl);

        return "/watermark";
    }

    /**
     * 做水印；
     * @return
     */
    @PostMapping(value = "/watermark2")
    public String doAndToWatermark2(@RequestParam(value = "image") MultipartFile[] images
            , HttpServletRequest request
            , Model model){
        String uploadPath = "/images";

        //S1-获取绝对路径；
        HttpSession session = request.getSession();
        String realUploadPath = session.getServletContext().getRealPath(uploadPath);
        this.logger.info("image={}",images);
        this.logger.info("realUploadPath={}",realUploadPath);

        if(images.length==0){
            this.logger.info("image={}",images);
        }
        List<Map<String,String>> resultData = new ArrayList<Map<String,String>>();
        //S2-循环上传图片及添加水印
        for(MultipartFile image:images){
            this.logger.info("image={},={}",image.isEmpty(),image.getSize());
            if(image.isEmpty()){
                continue;
            }

            Map<String,String> item = new HashMap<String,String>();
            //S2-上传图片；
            String imageUrl = uploadService.uploadImage(image,uploadPath,realUploadPath);

//        /*添加单个文字水印*/
//        String watermarkerImageUrl = this.singleMarkService.watermark(image,uploadPath,realUploadPath);

//        /*添加单个图片水印*/
//        String watermarkerImageUrl = this.imageMarkService.watermark(image,uploadPath,realUploadPath);

//        /*添加多个文字水印*/
//        String watermarkerImageUrl = this.moreMarkService.watermark(image,uploadPath,realUploadPath);

            /*添加多个图片水印*/
//            String watermarkerImageUrl = this.moreImageMarkService.watermark(image,uploadPath,realUploadPath);
            String watermarkerImageUrl = MarkServiceFactory.getMarkServiceByClassKey("MoreImage").watermark(image,uploadPath,realUploadPath);

            item.put("imageUrl",imageUrl);
            item.put("watermarkerImageUrl",watermarkerImageUrl);
            resultData.add(item);

        }

        model.addAttribute("imageData",resultData);
        this.logger.info("data={}",resultData);
        return "/watermark2";
    }

}
