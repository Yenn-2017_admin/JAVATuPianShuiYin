package com.example.watermark.service.impl;

import com.example.watermark.service.IMarkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.image.BufferedImage;

@Service
@Slf4j
public class MoreMarkService  extends AbstractMarkService implements IMarkService {



    public void draw(int width, int height, BufferedImage bufferedImage, Graphics2D graphics2D) {
        //            4、使用绘图工具，将水印（文字/图片）绘制到缓存图片对象
        graphics2D.setFont(new Font(FONT_NAME,FONT_STYLE,FONT_SIZE));
        graphics2D.setColor(FONT_COLOR);

        //S1-获取文字水印的宽、高
        int width1 = FONT_SIZE*getTextLength(MARK_TEXT);
        int height1 = FONT_SIZE;


        graphics2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,ALPHA));//设置水印图层

        graphics2D.rotate(Math.toRadians(30), bufferedImage.getHeight()/2, bufferedImage.getHeight()/2);//图层以中心为基点，转30度的弧度。

        int x = -width /2;
        int y = -height /2;

        while (x< width *1.5){
            y = -height /2;
            while (y< height *1.5){
                graphics2D.drawString(MARK_TEXT,x,y);
                y+=height1+MARGIN_ALL;
            }
            x+=width1+MARGIN_ALL;
        }


        graphics2D.dispose();
    }

}
