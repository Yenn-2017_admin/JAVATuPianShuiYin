package com.example.watermark.service.impl;

import com.example.watermark.service.IMarkService;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.image.BufferedImage;

@Service
public class SingleMarkService  extends AbstractMarkService implements IMarkService {



    public void draw(int width, int height, BufferedImage bufferedImage, Graphics2D graphics2D) {
        //            4、使用绘图工具，将水印（文字/图片）绘制到缓存图片对象
        graphics2D.setFont(new Font(FONT_NAME,FONT_STYLE,FONT_SIZE));
        graphics2D.setColor(FONT_COLOR);

        //S1-获取文字水印的宽、高
        int width1 = FONT_SIZE*getTextLength(MARK_TEXT);
        int height1 = FONT_SIZE;

        int widthDiff = width - width1;
        int heightDiff = height - height1;

        int x = X;
        int y = Y;
        if(x>widthDiff){
            x = widthDiff;
        }
        if(y>heightDiff){
            y = heightDiff;
        }
        graphics2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,ALPHA));//设置水印
        graphics2D.drawString(MARK_TEXT,x,y+FONT_SIZE);//x:横坐标，y:纵坐标
        graphics2D.dispose();
    }


}
