package com.example.watermark.service.impl;

import com.example.watermark.service.IMarkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Service
@Slf4j
public class MoreImageMarkService  extends AbstractMarkService implements IMarkService {


    public void draw(int width, int height, BufferedImage bufferedImage, Graphics2D graphics2D) throws IOException {
        //            4、使用绘图工具，将水印（文字/图片）绘制到缓存图片对象
        File logo = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "static/image/"+LOGO);

        log.info("logo={},is_exists={}",logo.getAbsolutePath(),logo.exists());

//            File logo = new File(logoPath);
        Image imageLogo = ImageIO.read(logo);
        int width1 = imageLogo.getWidth(null);
        int height1 = imageLogo.getHeight(null);


        graphics2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,ALPHA));//设置水印

        graphics2D.rotate(Math.toRadians(30), bufferedImage.getHeight()/2, bufferedImage.getHeight()/2);//图层以中心为基点，转30度的弧度。

        int x = -width /2;
        int y = -height /2;

        while (x< width *1.5){
            y = -height /2;
            while (y< height *1.5){
                graphics2D.drawImage(imageLogo,x,y,null);
                y+=height1+MARGIN_ALL;
            }
            x+=width1+MARGIN_ALL;
        }

        graphics2D.dispose();
    }
}
