package com.example.watermark.service.impl;

import com.example.watermark.service.IMarkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Service
@Slf4j
public class ImageMarkService extends AbstractMarkService implements IMarkService {



    public void draw(int width, int height, BufferedImage bufferedImage, Graphics2D graphics2D) throws IOException {
        //            4、使用绘图工具，将水印（文字/图片）绘制到缓存图片对象
        String path1 = ResourceUtils.CLASSPATH_URL_PREFIX;

        log.info("path1={},path={}",path1);

        File logo = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "static/image/"+LOGO);

        log.info("logo={},is_exists={}",logo.getAbsolutePath(),logo.exists());

//            File logo = new File(logoPath);
        Image imageLogo = ImageIO.read(logo);
        int width1 = imageLogo.getWidth(null);
        int height1 = imageLogo.getHeight(null);

        int widthDiff = width - width1;
        int heightDiff = height - height1;

        int x = X;
        int y = Y;
        if(x>widthDiff){
            x = widthDiff;
        }
        if(y>heightDiff){
            y = heightDiff;
        }

        graphics2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,ALPHA));//设置水印
        graphics2D.drawImage(imageLogo,x,y,null);//x:横坐标，y:纵坐标
        graphics2D.dispose();
    }
}
