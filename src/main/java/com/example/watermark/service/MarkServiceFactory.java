package com.example.watermark.service;

/**
 * @Author: Yenn
 * @Description: 水印的工厂类
 */
public class MarkServiceFactory {
    //?

    /**
     * 根据类的名称来生产对象
     * @param key
     * @return
     */
    public static IMarkService getMarkServiceByClassKey(String key){

        try {
            IMarkService markService = (IMarkService) Class.forName("com.example.watermark.service.impl."+key+"MarkService").newInstance();
            return markService;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
