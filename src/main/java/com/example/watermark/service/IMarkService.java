package com.example.watermark.service;

import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.io.File;

public interface IMarkService {
    public static final String MARK_TEXT = "LOGO123";

    public static final String FONT_NAME = "微软雅黑";
    public static final int FONT_STYLE = Font.BOLD;
    public static final int FONT_SIZE = 30;
    public static final int MARGIN_ALL = 50;
    public static final Color FONT_COLOR = Color.RED;

    public static final int X = 10;
    public static final int Y = 10;

    public static final float ALPHA = 0.3F;

    public static final String LOGO = "logo3.gif";

    public String watermark(MultipartFile image, String uploadPath, String realUploadPath);
}
