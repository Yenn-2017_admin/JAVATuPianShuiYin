package com.example.watermark.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class UploadService {

    public String uploadImage(MultipartFile image, String uploadPath, String realUploadPath){
        InputStream is = null;
        OutputStream os = null;

        File fileForRealUploadPath = new File(realUploadPath);
        if (!fileForRealUploadPath.exists()) {
            fileForRealUploadPath.mkdirs();
        }

        try {
            is = image.getInputStream();


            String des = realUploadPath + File.separator + image.getOriginalFilename();
            os = new FileOutputStream(des);

            byte[] buffer = new byte[1024];
            int len = 0;

            while ((len=is.read(buffer))>0){
                os.write(buffer);
            }
            os.flush();



        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if(is!=null){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(os!=null){
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return uploadPath+ File.separator + image.getOriginalFilename();
    }

    public List<String> uploadImage(MultipartFile[] images, String uploadPath, String realUploadPath){
        List<String> result = null;

        InputStream is = null;
        OutputStream os = null;

        File fileForRealUploadPath = new File(realUploadPath);
        if (!fileForRealUploadPath.exists()) {
            fileForRealUploadPath.mkdirs();
        }

        try {
            result = new ArrayList<String>();
            for (int i=0;i<images.length;i++){
                MultipartFile image = images[i];
                is = image.getInputStream();


                String des = realUploadPath + File.separator + image.getOriginalFilename();
                os = new FileOutputStream(des);

                byte[] buffer = new byte[1024];
                int len = 0;

                while ((len=is.read(buffer))>0){
                    os.write(buffer);
                }
                os.flush();
                result.add(uploadPath+ File.separator + image.getOriginalFilename());
            }




        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if(is!=null){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(os!=null){
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

}
